import { Component, OnInit } from '@angular/core';
import { contacts } from './contacts';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contacts = contacts;
  breakpoint: number = 3;

  constructor(breakpointObserver: BreakpointObserver) {
    breakpointObserver.observe([
      '(max-width: 1040px)',
      '(max-width: 780px)'
    ]).subscribe((state: BreakpointState) => {
      this.breakpoint = 3;

      if (state.breakpoints['(max-width: 1040px)']) {
        this.breakpoint = 2;
      }

      if (state.breakpoints['(max-width: 780px)']) {
        this.breakpoint = 1;
      }
    });
  }

  ngOnInit() {
  }

}
